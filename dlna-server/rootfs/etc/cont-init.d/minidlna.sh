#!/usr/bin/with-contenv bashio
# ==============================================================================
# minidlna.conf setup
# ==============================================================================

tempio \
    -conf /data/options.json \
    -template /usr/share/tempio/minidlnad_config \
    -out /etc/minidlna.conf