# Home Assistant Add-on: non-root SSH server

## Configuration

Add-on configuration:

```yaml
username: "nonroot"
authorized_keys:
  - "ssh-rsa AKDJD3839... my-key"
apks:
  - ffmpeg
  - python3
  - py3-pip
```

### Option: `username`

Name of the user.

### Option: `authorized_keys`

Your public keys that you wish to accept for login.

### Option: `apks`

Additional software packages to install in the add-on container.


## Network

The desired SSH TCP server port in the Network configuration input box. The standard port used for the SSH protocol is `22`.
