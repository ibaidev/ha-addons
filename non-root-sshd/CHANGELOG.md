# Changelog

## 0.0.2

- Adding media read/write permissions.

## 0.0.1

- Initial version.
