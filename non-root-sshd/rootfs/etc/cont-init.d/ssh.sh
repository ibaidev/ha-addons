#!/usr/bin/with-contenv bashio
# ==============================================================================
# SSH setup & user
# ==============================================================================

declare username
declare password

# Check if a username is set
bashio::config.require.username 'username'

# Warn about using the root user
if bashio::config.equals 'username' 'root'; then
    bashio::exit.nok "Login as root is not allowed!"
fi

# We require at least a password or an authorized key
if bashio::config.is_empty 'authorized_keys'
then
    bashio::exit.nok 'Please be sure to set a least a SSH password'
fi

username=$(bashio::config 'username')
username=$(bashio::string.lower "${username}")

# Create an user account
addgroup -S "${username}"
adduser -D -S "${username}" -G "${username}" -s "/bin/sh" \
    || bashio::exit.nok 'Failed creating the user account'    

# Use a random password in case none is set
password=$(pwgen 64 1)
chpasswd <<< "${username}:${password}" 2&> /dev/null

# Save history
touch /data/ash_history
chown ${username}:${username} /data/ash_history
ln -s /data/ash_history /home/${username}/.ash_history

# Sets up the authorized SSH keys
mkdir -p /home/${username}/.ssh
while read -r key; do
    echo "${key}" >> "/home/${username}/.ssh/authorized_keys"
done <<< "$(bashio::config 'authorized_keys')"
chown ${username}:${username} -R /home/${username}

# Generate config
mkdir -p /etc/ssh
tempio \
    -conf /data/options.json \
    -template /usr/share/tempio/sshd_config \
    -out /etc/ssh/sshd_config
